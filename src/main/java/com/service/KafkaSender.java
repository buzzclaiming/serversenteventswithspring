package com.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@Service
public class KafkaSender {

    private static final Logger LOG = LoggerFactory.getLogger(KafkaSender.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Value("${app.topic.foo}")
    private String topic;

    public void send() {


        while(true){


            try(BufferedReader reader = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("shipsSimulation.json")))){

                String line = reader.readLine();
                while (line != null) {

                    kafkaTemplate.send(topic,line);
                    LOG.info("sending message='{}' to topic='{}'", line, topic);
                    Thread.sleep(1000 * 5);
                    line = reader.readLine();
                }
            }catch (Exception e){
                LOG.error(e.getLocalizedMessage(),e);
            };
        }
    }
}